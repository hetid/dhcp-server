#include <stdio.h>
#include <stdint.h>
#include <pthread.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <sys/msg.h>
#include <sys/ipc.h>
#include <string.h>
#include <signal.h>
#include <time.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <net/if.h>

#define UDP_SERVER_PORT        67	//send to server (67 RFC, permission denied)
#define UDP_CLIENT_PORT        68	//send to client (68 RFC, permission denied)
#define NUMBER_OF_CLIENTS      8
#define MAGIC_COOKIE           "99.130.83.99" //in decimal 
#define MTU                    1500 //bytes
#define IP_IN_CHAR_LEN         16

#define SLEEP_TIME             3    //secs
#define WAITING_CONVERS_TIME   15
#define WAITING_LEASER_TIME    86400

#define REQUESTED_IP_ADDRESS   50
#define IP_ADDRESS_LEASE_TIME  51
#define DHCP_MESSAGE_TYPE      53
#define ETHERNET_ADDRESS_LEN   18

#define DHCP_DISCOVER          1
#define DHCP_OFFER             2
#define DHCP_REQUEST           3
#define DHCP_DECLINE           4
#define DHCP_ACK               5
#define DHCP_NAK               6
#define DHCP_RELEASE           7
#define DHCP_INFORM            8
#define END_OPTIONS            255

#define DEFAULT_INTERFACE      "eth0"

enum opField{
	BOOTREQUEST = 1,
	BOOTREPLY
};

struct dhcpHeader{
	uint8_t op;
	uint8_t htype;
	uint8_t hlen;
	uint8_t hops;
	uint32_t xid;
	uint16_t secs;
	uint16_t flags;
	struct in_addr ciaddr; 
	struct in_addr yiaddr; 
	struct in_addr siaddr; 
	struct in_addr giaddr;
	unsigned char chaddr[ETHERNET_ADDRESS_LEN];
	unsigned char sname[64];
	unsigned char file[128];
	uint8_t options[256];
}__attribute__((packed));

struct requestedIpAddress{
	uint8_t tag;
	uint8_t len;
	unsigned char data[4];
}__attribute__((packed));

struct dhcpMessageType{
	uint8_t tag;
	uint8_t len;
	unsigned char data;
}__attribute__((packed));

struct leaseTime{
	uint8_t tag;
	uint8_t len;
	unsigned char data[4];
}__attribute__((packed));

enum states{
	FAILEDSTATE = -1,
	INIT = 0,
	IN_CONVERS,
	LEASER
};

enum signals{
	DHCPDISCOVER = 1,
	DHCPREQUEST = 3,
	DHCPDECLINE = 4,
	DHCPRELEASE = 7,
	DHCPINFORM = 8
};

struct matchPair{
	enum states state;
	enum signals signal;
};


struct timer{
	time_t timeOut;
	uint32_t xid;
	enum states state;
};

struct elementFSM{
	enum states status;
	struct dhcpHeader header;
};

typedef void (*transmitionFunc)(struct elementFSM *arg);

struct transitionState{
	enum states newState;
	transmitionFunc newFunc;
};


void init_to_convers(struct elementFSM*);
void convers_to_leaser(struct elementFSM*);
void make_new_offer(struct elementFSM*);
void leaser_to_init(struct elementFSM*);
/////////////////////////////////////////////////////////////////////////////////////////
//    match table. view:                                                               //
//    [current state][signal to change state] = {new state, change state function},    //
/////////////////////////////////////////////////////////////////////////////////////////
struct transitionState matchTable[3][9] = {
	[INIT][DHCPDISCOVER] = {IN_CONVERS, init_to_convers},
	[INIT][DHCPREQUEST] = {INIT, NULL},
	[INIT][DHCPDECLINE] = {INIT, NULL},
	[INIT][DHCPRELEASE] = {INIT, NULL},
	[INIT][DHCPINFORM] = {INIT, NULL},
	[IN_CONVERS][DHCPDISCOVER] = {IN_CONVERS, NULL},
	[IN_CONVERS][DHCPREQUEST] = {LEASER, convers_to_leaser},
	[IN_CONVERS][DHCPDECLINE] = {IN_CONVERS, make_new_offer},
	[IN_CONVERS][DHCPRELEASE] = {IN_CONVERS, NULL},
	[IN_CONVERS][DHCPINFORM] = {IN_CONVERS, NULL},
	[LEASER][DHCPDISCOVER] = {LEASER, NULL},
	[LEASER][DHCPREQUEST] = {LEASER, NULL},
	[LEASER][DHCPDECLINE] = {LEASER, NULL},
	[LEASER][DHCPRELEASE] = {INIT, leaser_to_init},
	[LEASER][DHCPINFORM] = {LEASER, NULL}
};

struct msgIdentifier{
	uint64_t type;
	uint8_t flag;
};

struct clients{
	enum states statusFSM;
	uint32_t transactionId;
	unsigned char hardWareAddress[ETHERNET_ADDRESS_LEN];
	unsigned char ipAddress[16];
	time_t timeStamp;
	struct msgIdentifier msgtyp;
}__attribute__((packed));

unsigned char ipPool[NUMBER_OF_CLIENTS][16] = {
	"192.168.0.2",
	"192.168.0.3",
	"192.168.0.4",
	"192.168.0.5",
	"192.168.0.6",
	"192.168.0.7",
	"192.168.0.8",
	"192.168.0.9"
};

struct msgbuf{
	long mtype;
	struct elementFSM mtext;
	transmitionFunc func;
};