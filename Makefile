run: server test_client
server: server.o test_client.o
	gcc -o server server.o -lpthread
	gcc -o test_client test_client.o
server.o: server.c dhcp_msg.h test_client.c
	gcc -std=gnu99 -c server.c
	gcc -std=gnu99 -c test_client.c
clean:
	rm server server.o test_client test_client.o
