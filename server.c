#include "dhcp_msg.h"

void* receive_thread_udp(void* arg);
void* transmit_thread_udp(void* arg);
void* configuration_func(void* arg);
struct transitionState determining_state(struct dhcpHeader *msgReceived);

int send_to_msq(struct elementFSM *tempElement, transmitionFunc newFunc, int MsqId);
int recv_from_msq(struct elementFSM *tempElement, transmitionFunc *newFunc, int MsqId, int typ);
void add_new_client(struct elementFSM *definiteElementFSM);
int options_filling(uint8_t oldMessageType, uint8_t newMessageType, struct dhcpHeader *msgReceived);
void add_end_of_options(struct dhcpHeader *msgReceived, int offset);

void time_update(uint32_t xid);
void* time_check(void* arg);
void temp_delay();
int check_in_queue(uint32_t xid);

pthread_mutex_t ipPoolMutex;
pthread_mutex_t queueMutex;

int toConfMsqId, fromConfMsqId;
struct clients clientsTable[NUMBER_OF_CLIENTS];

/***************************************************************************************************************************************
* Эта программа иммитирует работу DHCP-сервера (построенного по принципу машины конечных состояний - FSM).                             *
* В функции main() создаются 3 потока:                                                                                                 *
*	receive_thread_udp отвечает за приём UDP-пакетов, содержащих запросы (BOOTREQUEST) протокола DHCP;                                 *
*	configuration_func отвечает за обработку принятых пакетов и формирование ответов;                                                  *
*	transmit_thread_udp отвечает за отправку сформированных пакетов (BOOTREPLY).                                                       *
* Клиент (в контексте сервера) может принимать 3 состояния: INIT, IN_CONVERS и LEASER;                                                 *
* а так же 8 "сигналов" о смене состояния: DHCP_DISCOVER, DHCP_OFFER, DHCP_REQUEST, DHCP_DECLINE,                                      *
*                                          DHCP_ACK, DHCP_NAK, DHCP_RELEASE, DHCP_INFORM.                                              *
* Массив matchTable[][] описывает варианты взаимодействий между текущим состоянием и сигналом, пришедшим в DHCP-пакете.                *
****************************************************************************************************************************************
* This program imitates the operation of the DHCP-server (built on the principle of the machine of final states - FSM).                *
* In the main () function, 3 threads are created:                                                                                      *
*	receive_thread_udp is responsible for receiving UDP-packets containing DHCP-requests (BOOTREQUEST);                                *
*	configuration_func is responsible for processing received packets and generating responses;                                        *
*	transmit_thread_udp is responsible for sending the generated packets (BOOTREPLY).                                                  *
* The client (in the context of the server) can be in 3 states: INIT, IN_CONVERS and LEASER;                                           *
* as well as 8 "signals" about changing the state: DHCP_DISCOVER, DHCP_OFFER, DHCP_REQUEST, DHCP_DECLINE,                              *
*                                                  DHCP_ACK, DHCP_NAK, DHCP_RELEASE, DHCP_INFORM.                                      *
* The matchTable[][] array describes the interaction options between the current state and the signal that came in the DHCP-packet.    *
***************************************************************************************************************************************/
int main(int argc, char const *argv[]){
	pthread_t receiveThreadId;
	pthread_t transmitThreadId;
	pthread_t configThreadId;

	toConfMsqId = msgget(IPC_PRIVATE, IPC_CREAT | 0600);
	fromConfMsqId = msgget(IPC_PRIVATE, IPC_CREAT | 0600);
	
	int recvThreadRes = pthread_create(&receiveThreadId, 0, receive_thread_udp, NULL);
	if (recvThreadRes != 0){
		perror("FAILED (create receive_thread");
	}
	int tsmtThreadRes = pthread_create(&transmitThreadId, 0, transmit_thread_udp, NULL);
	if (tsmtThreadRes != 0){
		perror("FAILED (create transmit_thread");		
	}
	for (int i = 0; i < NUMBER_OF_CLIENTS; ++i){
		int cnfgThreadRes = pthread_create(&configThreadId, 0, configuration_func, NULL);
		if (cnfgThreadRes != 0){
			perror("FAILED (create config_thread");
		}
	}
	pthread_join(receiveThreadId, NULL);
	perror("FAILED GLOBAL (receive interrupted)...\n");

	return 0;
}
void* receive_thread_udp(void* arg){
	struct dhcpHeader dhcpMsgReceived;
	struct elementFSM tempElementFSM;
	struct ifreq interface;
	struct transitionState status;
	int bytesReceived = 0;
	int n = 1;
	int rxSockDesk = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (rxSockDesk <= 0){
		perror("FAILED (craete rxSocket)\n");
	}

	const int trueValue = 1;
	setsockopt(rxSockDesk, SOL_SOCKET, SO_REUSEADDR, &trueValue, sizeof(trueValue));
	
	if (setsockopt(rxSockDesk, SOL_SOCKET, SO_BROADCAST, (char *) &n, sizeof(n)) == -1) {
		close(rxSockDesk);
		pthread_exit(NULL);
	}

	struct sockaddr_in rxSockAddr;
	rxSockAddr.sin_family = AF_INET;
	rxSockAddr.sin_addr.s_addr = htonl(INADDR_BROADCAST);
	rxSockAddr.sin_port = htons(UDP_SERVER_PORT);

	if (setsockopt(rxSockDesk, SOL_SOCKET, SO_REUSEADDR, (char *) &n, sizeof(n)) == -1) {
		close(rxSockDesk);
		pthread_exit(NULL);
	}
	if (bind(rxSockDesk, (const struct sockaddr*) &rxSockAddr, sizeof(struct sockaddr_in)) < 0){
		perror("FAILED (bind rxSocket)\n");
	}

	pthread_mutex_lock(&ipPoolMutex);
	for(int i = 0; i < NUMBER_OF_CLIENTS; i++){
		memset(&clientsTable[i], 0, sizeof(struct clients));
	}
	pthread_mutex_unlock(&ipPoolMutex);

	while(1){
		if ((bytesReceived = recvfrom(rxSockDesk, &dhcpMsgReceived , sizeof(dhcpMsgReceived), 0, NULL, NULL)) == -1){
			perror("FAILED (recvfrom)\n");
			continue;
		}
				printf("in while\n");

		check_in_queue(dhcpMsgReceived.xid);
		status = determining_state(&dhcpMsgReceived);
		if (status.newState == FAILEDSTATE){
			perror("FAILED ( determining_state() )\n");
			continue;
		}
printf("recvfrom p = %p\n", status.newFunc);
		tempElementFSM.status = status.newState;
		tempElementFSM.header = dhcpMsgReceived;
		if (send_to_msq(&tempElementFSM, status.newFunc, toConfMsqId) == -1){
			continue;
		}
	}
	pthread_exit(0);
}

void* configuration_func(void* arg){
	pthread_detach(pthread_self());
	int typ = 0;
	struct elementFSM definiteElementFSM;
	transmitionFunc *newFunc = calloc(1, sizeof(transmitionFunc));
	while(1){
		if (typ == 0){
			//definition of the type of message to be retrieved
			pthread_mutex_lock(&ipPoolMutex);
			for(int i = 0; i < NUMBER_OF_CLIENTS; i++){
				if (clientsTable[i].msgtyp.flag == 0){
					typ = clientsTable[i].msgtyp.type;
					clientsTable[i].msgtyp.flag = 1;
					break;
				}		
			}
			pthread_mutex_unlock(&ipPoolMutex);
		}

		pthread_mutex_lock(&queueMutex);
		if (recv_from_msq(&definiteElementFSM, newFunc, toConfMsqId, typ) < 0){
			pthread_mutex_unlock(&queueMutex);
			continue;
		}
		pthread_mutex_unlock(&queueMutex);

			printf("*newFunc = %p, leaser_to_init = %p\n", *newFunc, init_to_convers);
		if (*newFunc != NULL){
			(*newFunc)(&definiteElementFSM);
			printf("CALL\n");
		}else printf("packed was dropped... in configuration_func()\n");
	}
	free(newFunc);
	pthread_exit(0);
}

void* transmit_thread_udp(void* arg){
	pthread_detach(pthread_self());
	struct elementFSM txClientFSM;
	int n = 1;
	int txSockDesk = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (txSockDesk <= 0){
		perror("FAILED (craete txSocket)\n");
	}
	if (setsockopt(txSockDesk, SOL_SOCKET, SO_BROADCAST, (char *) &n, sizeof(n)) == -1) {
		close(txSockDesk);
		pthread_exit(NULL);
	}

	struct sockaddr_in txSockAddr;
	txSockAddr.sin_family = AF_INET;
	txSockAddr.sin_port = htons(UDP_CLIENT_PORT);
	txSockAddr.sin_addr.s_addr = htonl(INADDR_BROADCAST);

	while(1){
		if (recv_from_msq(&txClientFSM, NULL, fromConfMsqId, 0) < 0){
			continue;
		}		
		if(sendto(txSockDesk, &txClientFSM.header, sizeof(struct dhcpHeader), 0, (struct sockaddr *)&txSockAddr, sizeof(txSockAddr)) == -1){
			perror("FAILED (sendto())");
		}
	}
	pthread_exit(0);
}

struct transitionState determining_state(struct dhcpHeader *msgReceived){
	struct matchPair pair;
	int ciaddrValue;
	int offset = sizeof(uint32_t);    //offset for magic cookie
	uint8_t tag = 0;
	uint8_t len = 0;
	uint8_t dataMsgType = 0;
	enum states newState;
	struct transitionState status = {FAILEDSTATE, NULL};

	if (msgReceived == NULL){
		perror("FAILED (msgReceived pointer is NULL)");
		status.newState = FAILEDSTATE;
		return status;
	}
	if (msgReceived->op != BOOTREQUEST){
		status.newState = FAILEDSTATE;
		return status;
	}

	pair.state = INIT;
	//check state at MAC-address
	pthread_mutex_lock(&ipPoolMutex);
	for (int i = 0; i < NUMBER_OF_CLIENTS; ++i){
		printf("MAC[%d] - %s\n", i, clientsTable[i].hardWareAddress);
		if (strcmp(msgReceived->chaddr, clientsTable[i].hardWareAddress) == 0){
			pair.state = clientsTable[i].statusFSM;
			break;
		}
	}
	pthread_mutex_unlock(&ipPoolMutex);

	while(1){    //Search option with tag DHCP_MESSAGE_TYPE
		tag = *(msgReceived->options + offset);
		if (tag == DHCP_MESSAGE_TYPE){
			offset += sizeof(tag) + sizeof(len);
			dataMsgType = *(msgReceived->options + offset);
			break;
		}
		else{
			offset += sizeof(tag);
			len = *(msgReceived->options + offset);
			offset += sizeof(len) + len;
		}
	}
	pair.signal = (enum signals)dataMsgType;
printf("state^ %d\n", pair.state);
	//definition of a new state and transition function by matchTable[][]
	newState = matchTable[pair.state][pair.signal].newState;
	status.newFunc = matchTable[pair.state][pair.signal].newFunc;
	status.newState = newState;

	return status;
}

int send_to_msq(struct elementFSM *tempElement, transmitionFunc newFunc, int MsqId){
	struct msgbuf n;
	n.mtype = tempElement->header.xid;
	n.mtext = *tempElement;
	n.func = newFunc;
	if (msgsnd(MsqId, &n, sizeof(n), 0) < 0){
		perror("FAILED ( send_to_msq() )");
		return -1;
	}
	return 0;
}

int recv_from_msq(struct elementFSM *tempElement, transmitionFunc *newFunc, int MsqId, int typ){
	struct msgbuf n;
	if (msgrcv(MsqId, &n, sizeof(n), typ, 0) < 0){
		perror("FAILED ( recv_from_msq() )");
		return -1;
	}
	*tempElement = n.mtext;
	if (newFunc != NULL){
		*newFunc = n.func;
	}
	return 0;
}

void add_new_client(struct elementFSM *definiteElementFSM){
	struct elementFSM newClientFSM = *definiteElementFSM;
	struct timer *tempTimer = calloc(1, sizeof(struct timer));
		
	pthread_mutex_lock(&ipPoolMutex);
	for (int i = 0; i < NUMBER_OF_CLIENTS; ++i){
		printf("xid[i] :_%ld\n", clientsTable[i].msgtyp.type);
		if (clientsTable[i].statusFSM == INIT && clientsTable[i].msgtyp.type == newClientFSM.header.xid){
			clientsTable[i].statusFSM = newClientFSM.status;
			clientsTable[i].transactionId = newClientFSM.header.xid;
			strcpy(clientsTable[i].hardWareAddress, newClientFSM.header.chaddr);
			printf("КОПироваНО!\n");
			break;
		}
	}
	pthread_mutex_unlock(&ipPoolMutex);

	time_update(newClientFSM.header.xid);
	tempTimer->timeOut = WAITING_CONVERS_TIME;
	tempTimer->xid = newClientFSM.header.xid;
	tempTimer->state = IN_CONVERS;
	pthread_t pid;
	int timerRes = pthread_create(&pid, 0, time_check, (void*)tempTimer);
	if (timerRes != 0){
		perror("FAILED (create timer in add_new_client()");
	}
}

int options_filling(uint8_t oldMessageType,uint8_t newMessageType, struct dhcpHeader *msgReceived){
	int offset = sizeof(uint32_t);    //magic cookie offset
	uint8_t tag = 0;
	uint8_t len = 0;
	uint8_t dataMsgType = 0;

	while(1){
		tag = *(msgReceived->options + offset);
		if (tag == DHCP_MESSAGE_TYPE){
			offset += sizeof(tag) + sizeof(len);
			dataMsgType = *(msgReceived->options + offset);
			if (dataMsgType == oldMessageType){
				memccpy(msgReceived->options + offset, &newMessageType, ' ', sizeof(uint8_t));
				offset += sizeof(newMessageType);
				add_end_of_options(msgReceived, offset);
				break;
			}
		}
		else{
			offset += sizeof(tag);
			len = *(msgReceived->options + offset);
			offset += sizeof(len) + len;
		}
	}
	return 0;
}

void init_to_convers(struct elementFSM *arg){
	struct elementFSM clientFSM = *arg;
	unsigned char tempIpStr[IP_IN_CHAR_LEN] = {0};
	int counter = 0;
	struct in_addr correlationIpAddress;

	
	add_new_client(&clientFSM);
	//search for a free address
	pthread_mutex_lock(&ipPoolMutex);
	for (int i = 0; i < NUMBER_OF_CLIENTS; ++i){
		counter = 0;
		for (int j = 0; j < NUMBER_OF_CLIENTS; ++j){
			if (strcmp(ipPool[i], clientsTable[j].ipAddress) != 0){
				counter++;
			}
		}
		if (counter == NUMBER_OF_CLIENTS){
			strcpy(tempIpStr, ipPool[i]);
			break;
		}
	}
	//correlating client - ip_address
	for (int i = 0; i < NUMBER_OF_CLIENTS; ++i){
		if (clientsTable[i].transactionId == clientFSM.header.xid){
			strcpy(clientsTable[i].ipAddress, tempIpStr);
		}
	}
	pthread_mutex_unlock(&ipPoolMutex);

	//header filling
	inet_pton(AF_INET, tempIpStr, &correlationIpAddress);
	memccpy(&clientFSM.header.yiaddr, &correlationIpAddress, ' ', sizeof(struct in_addr));
	clientFSM.header.op = BOOTREPLY;
	clientFSM.header.secs = 0;

	options_filling((uint8_t)DHCP_DISCOVER,(uint8_t)DHCP_OFFER, &clientFSM.header);

	send_to_msq(&clientFSM, NULL, fromConfMsqId);
}

void convers_to_leaser(struct elementFSM* arg){
	struct elementFSM clientFSM = *arg;
	unsigned char tempIpStr[IP_IN_CHAR_LEN] = {0};
	int counter = 0;
	struct in_addr correlationIpAddress;

	struct timer *tempTimer = calloc(1, sizeof(struct timer));
	tempTimer->timeOut = WAITING_LEASER_TIME;
	tempTimer->xid = clientFSM.header.xid;
	tempTimer->state = LEASER;
	pthread_t pid;
	int timerRes = pthread_create(&pid, 0, time_check, (void*)tempTimer);
	if (timerRes != 0){
		perror("FAILED (create timer in convers_to_leaser()");
	}

	pthread_mutex_lock(&ipPoolMutex);
	//correlating client-ip_address
	for (int i = 0; i < NUMBER_OF_CLIENTS; ++i){
		if (clientsTable[i].transactionId == clientFSM.header.xid){
			clientsTable[i].timeStamp = time(NULL);
			clientsTable[i].statusFSM = LEASER;
			strcpy(tempIpStr, clientsTable[i].ipAddress);
		}
	}

	pthread_mutex_unlock(&ipPoolMutex);
	//header filling
	inet_pton(AF_INET, tempIpStr, &correlationIpAddress);
	memccpy(&clientFSM.header.yiaddr, &correlationIpAddress, ' ', sizeof(struct in_addr));
	clientFSM.header.op = BOOTREPLY;
	clientFSM.header.secs = 0;

	options_filling((uint8_t)DHCP_REQUEST,(uint8_t)DHCP_ACK, &clientFSM.header);
	send_to_msq(&clientFSM, NULL, fromConfMsqId);
	printf("add leaser.\n");
}

void make_new_offer(struct elementFSM* arg){
	struct elementFSM clientFSM = *arg;
	unsigned char tempIpStr[IP_IN_CHAR_LEN] = {0};
	int counter = 0;
	struct in_addr correlationIpAddress;

	pthread_mutex_lock(&ipPoolMutex);
	//search for a free address
	for (int i = 0; i < NUMBER_OF_CLIENTS; ++i){
		if (clientsTable[i].transactionId == clientFSM.header.xid){
			clientsTable[i].timeStamp = time(NULL);
		}
		counter = 0;
		for (int j = 0; j < NUMBER_OF_CLIENTS; ++j){
			if (strcmp(ipPool[i], clientsTable[j].ipAddress) != 0){
				counter++;
			}
		}
		if (counter == NUMBER_OF_CLIENTS){
			strcpy(tempIpStr, ipPool[i]);
			break;
		}
	}
	//correlating client-ip_address
	for (int i = 0; i < NUMBER_OF_CLIENTS; ++i){
		if (clientsTable[i].transactionId == clientFSM.header.xid){
			strcpy(clientsTable[i].ipAddress, tempIpStr);
		}
	}
	pthread_mutex_unlock(&ipPoolMutex);
	//header filling
	inet_pton(AF_INET, tempIpStr, &correlationIpAddress);
	memccpy(&clientFSM.header.yiaddr, &correlationIpAddress, ' ', sizeof(struct in_addr));
	clientFSM.header.op = BOOTREPLY;
	clientFSM.header.secs = 0;

	options_filling((uint8_t)DHCP_DECLINE,(uint8_t)DHCP_OFFER, &clientFSM.header);
	send_to_msq(&clientFSM, NULL, fromConfMsqId);
}

void leaser_to_init(struct elementFSM* arg){
	struct elementFSM clientFSM = *arg;

	pthread_mutex_lock(&ipPoolMutex);
	for (int i = 0; i < NUMBER_OF_CLIENTS; ++i){
		if (clientsTable[i].transactionId == clientFSM.header.xid){
			memset(&clientsTable[i], 0, sizeof(struct clients));
		}
	}
	pthread_mutex_unlock(&ipPoolMutex);
}

void time_update(uint32_t xid){
	pthread_mutex_lock(&ipPoolMutex);
	for (int i = 0; i < NUMBER_OF_CLIENTS; ++i){
		if (clientsTable[i].transactionId == xid){
			clientsTable[i].timeStamp = time(NULL);
		}
	}
	pthread_mutex_unlock(&ipPoolMutex);
}

void* time_check(void* arg){
	pthread_detach(pthread_self());

	struct timer temperTimer = *(struct timer*)arg;
	free(arg);

	while(1){
		sleep(SLEEP_TIME);
		pthread_mutex_lock(&ipPoolMutex);
		for (int i = 0; i < NUMBER_OF_CLIENTS; ++i){
			if (clientsTable[i].transactionId == temperTimer.xid && clientsTable[i].statusFSM == temperTimer.state && temperTimer.xid != 0){
				if ((time(NULL) - clientsTable[i].timeStamp) >= temperTimer.timeOut){
					memset(&clientsTable[i], 0, sizeof(struct clients));
					pthread_mutex_unlock(&ipPoolMutex);
					pthread_exit(0);
				}else break;
			}
		}
		pthread_mutex_unlock(&ipPoolMutex);
	}
	pthread_exit(0);
}

void add_end_of_options(struct dhcpHeader *msgReceived, int offset){
	uint8_t data = END_OPTIONS;
	memccpy(msgReceived->options + offset, &data, ' ', sizeof(uint8_t));
}

void temp_delay(){
	uint32_t randomValue;
	struct timeval tempTime;
	settimeofday(&tempTime , NULL);
	srand(tempTime.tv_usec);
	//srand(time(NULL));
	randomValue = rand()%5;
	printf("sleep(%d)\n",randomValue);
	sleep(randomValue);
}

int check_in_queue(uint32_t xid){
	pthread_mutex_lock(&ipPoolMutex);
	for(int i = 0; i < NUMBER_OF_CLIENTS; i++){
		if (clientsTable[i].msgtyp.type == xid){
			pthread_mutex_unlock(&ipPoolMutex);
			return -1;
		}		
	}
	for(int i = 0; i < NUMBER_OF_CLIENTS; i++){
		if (clientsTable[i].msgtyp.type == 0){
			clientsTable[i].msgtyp.type = xid;
			break;
		}		
	}
	pthread_mutex_unlock(&ipPoolMutex);
	return 0;
}