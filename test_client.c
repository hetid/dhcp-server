#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdint.h>
#include <netinet/in.h>
#include <net/if.h>
#include <pthread.h>
#include <signal.h>
#include <string.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/msg.h>
#include <sys/ipc.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <time.h>
#include <unistd.h>

#define UDP_SERVER_PORT        67	//send to server (67 RFC, permission denied)
#define UDP_CLIENT_PORT        68	//send to client (68 RFC, permission denied)
#define ETHERNET_ADDRESS_TYPE  0x01     //hlen=6 for ethernet address (MAC)
#define ETHERNET_ADDRESS_LEN   18       //length for MAC-address
#define REQUESTED_IP_ADDRESS   50
#define IP_ADDRESS_LEASE_TIME  51
#define DHCP_MESSAGE_TYPE      53
#define DHCP_LENGTH_TYPE       1
#define END_OPTIONS            255
#define DEFAULT_INTERFACE      "eth0"

struct dhcpHeader{
	uint8_t op;
	uint8_t htype;
	uint8_t hlen;
	uint8_t hops;
	uint32_t xid;
	uint16_t secs;
	uint16_t flags;
	struct in_addr ciaddr; 
	struct in_addr yiaddr; 
	struct in_addr siaddr; 
	struct in_addr giaddr;
	unsigned char chaddr[ETHERNET_ADDRESS_LEN];
	unsigned char sname[64];
	unsigned char file[128];
	uint8_t options[256];
}__attribute__((packed));

enum opField{
	BOOTREQUEST = 1,
	BOOTREPLY
};

struct dhcpMessageType{
	uint8_t tag;
	uint8_t len;
	unsigned char data;
}__attribute__((packed));

enum signals{
	DHCPDISCOVER = 1,
	DHCPREQUEST = 3,
	DHCPDECLINE = 4,
	DHCPRELEASE = 7,
	DHCPINFORM = 8
};

uint8_t MAGIC_COOKIE[4] = {99, 130, 83, 99};



int filling_dhcp_header(enum signals messageType, struct dhcpHeader *txClientFSM, uint32_t xid, unsigned char *mac);
int udp_tx_socket_creating(int *txSockDesk, struct sockaddr_in *txSockAddr);
int udp_rx_socket_creating(int *rxSockDesk, struct sockaddr_in *rxSockAddr);
uint32_t generating_transaction_id();
void get_mac_address(unsigned char *mac);
int check_mac_address(unsigned char *mac, unsigned char *recvMac);
void temp_delay();


int main(int argc, char const *argv[]){
	struct dhcpHeader txClientFSM = {0};
	struct dhcpHeader dhcpMsgReceived = {0};
	struct sockaddr_in txSockAddr;
	struct sockaddr_in rxSockAddr;
	int txSockDesk;
	int rxSockDesk;
	uint32_t xid;
	unsigned char mac[ETHERNET_ADDRESS_LEN] = {0};
	udp_tx_socket_creating(&txSockDesk, &txSockAddr);
	udp_rx_socket_creating(&rxSockDesk, &rxSockAddr);
	get_mac_address(mac);
	xid = generating_transaction_id();
	printf("mac = %s\n", mac);
	printf("xid = %d\n", xid);
	
	filling_dhcp_header(DHCPDISCOVER, &txClientFSM, xid, mac);

	if (sendto(txSockDesk, &txClientFSM, sizeof(struct dhcpHeader), 0, (struct sockaddr *)&txSockAddr, sizeof(txSockAddr)) == -1){
		perror("FAILED (sendto())");
	}

	while (1){
		if ((recvfrom(rxSockDesk, &dhcpMsgReceived , sizeof(dhcpMsgReceived), 0, NULL, NULL)) == -1){
			perror("FAILED (recvfrom)\n");
		}
		if (check_mac_address(mac, dhcpMsgReceived.chaddr) == 0){
			break;
		}
	}

	printf("IP_address may be %s [DHCPOFFER]\n", inet_ntoa(dhcpMsgReceived.yiaddr));
	temp_delay();
	filling_dhcp_header(DHCPREQUEST, &txClientFSM, xid, mac);
	
	if (sendto(txSockDesk, &txClientFSM, sizeof(struct dhcpHeader), 0, (struct sockaddr *)&txSockAddr, sizeof(txSockAddr)) == -1){
		perror("FAILED (sendto())");
	}
	
	while (1){
		if ((recvfrom(rxSockDesk, &dhcpMsgReceived , sizeof(dhcpMsgReceived), 0, NULL, NULL)) == -1){
			perror("FAILED (recvfrom)\n");
		}
		if (check_mac_address(mac, dhcpMsgReceived.chaddr) == 0){
			break;
		}
	}
	printf("mac = %s\n", mac);
	printf("xid = %d\n", xid);
	printf("IP_address is %s [DHCPACK]\n", inet_ntoa(dhcpMsgReceived.yiaddr));
	close(txSockDesk);
	close(rxSockDesk);
	return 0;
}

int filling_dhcp_header(enum signals messageType, struct dhcpHeader *txClientFSM,uint32_t xid, unsigned char *mac){
	txClientFSM->op = BOOTREQUEST;
	txClientFSM->htype = ETHERNET_ADDRESS_TYPE;
	txClientFSM->hlen = ETHERNET_ADDRESS_LEN;
	txClientFSM->xid = xid;
	strcpy(txClientFSM->chaddr, mac);
	
	//begin MAGIC COOKIE
	/*txClientFSM.options[0] = 99;
	txClientFSM.options[1] = 130;
	txClientFSM.options[2] = 83;
	txClientFSM.options[3] = 99;*/
	memcpy(txClientFSM->options, MAGIC_COOKIE, sizeof(MAGIC_COOKIE));
	//end MAGIC COOKIE
	txClientFSM->options[4] = DHCP_MESSAGE_TYPE;
	txClientFSM->options[5] = DHCP_LENGTH_TYPE;
	txClientFSM->options[6] = messageType;
	txClientFSM->options[7] = END_OPTIONS;

	return 0;
}

uint32_t generating_transaction_id(){
	uint32_t randomValue;
	struct timeval tempTime;
	settimeofday(&tempTime , NULL);
	srand(tempTime.tv_usec);
	randomValue = rand();
	return randomValue;
}

int udp_tx_socket_creating(int *txSockDesk, struct sockaddr_in *txSockAddr){
	int n = 1;
	struct ifreq interface;

	*txSockDesk = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (*txSockDesk <= 0){
		perror("FAILED (craete txSocket)\n");
		return -1;
	}
	if (setsockopt(*txSockDesk, SOL_SOCKET, SO_BROADCAST, (char *) &n, sizeof(n)) == -1) {
		close(*txSockDesk);
		return -1;
	}

	txSockAddr->sin_family = AF_INET;
	txSockAddr->sin_addr.s_addr = htonl(INADDR_BROADCAST);
	txSockAddr->sin_port = htons(UDP_SERVER_PORT);

	/*if (setsockopt(*txSockDesk, SOL_SOCKET, SO_REUSEADDR, (char *) &n, sizeof(n)) == -1) {
		close(*txSockDesk);
		return -1;
	}*/

	return 0;
}

int udp_rx_socket_creating(int *rxSockDesk, struct sockaddr_in *rxSockAddr){
	int n = 1;
	*rxSockDesk = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (*rxSockDesk <= 0){
		perror("FAILED (craete rxSocket)\n");
		return -1;
	}
	if (setsockopt(*rxSockDesk, SOL_SOCKET, SO_BROADCAST, (char *) &n, sizeof(n)) == -1) {
		close(*rxSockDesk);
		return -1;
	}

	rxSockAddr->sin_family = AF_INET;
	rxSockAddr->sin_addr.s_addr = htonl(INADDR_BROADCAST);
	rxSockAddr->sin_port = htons(UDP_CLIENT_PORT);

	if (setsockopt(*rxSockDesk, SOL_SOCKET, SO_REUSEADDR, (char *) &n, sizeof(n)) == -1) {
		close(*rxSockDesk);
		return -1;
	}

	if (bind(*rxSockDesk, (const struct sockaddr*) rxSockAddr, sizeof(struct sockaddr_in)) < 0){
		perror("FAILED (bind rxSocket)\n");
		return -1;
	}
	return 0;
}

void get_mac_address(unsigned char *mac){
	FILE *fp = fopen("/sys/class/net/eth0/address", "r"); //eth0
	fgets(mac, ETHERNET_ADDRESS_LEN, fp);
	fclose(fp);
}

int check_mac_address(unsigned char *mac, unsigned char *recvMac){
	if (strcmp(mac,recvMac) != 0){
		return -1;
	}
	return 0;
}

void temp_delay(){
	uint32_t randomValue;
	struct timeval tempTime;
	settimeofday(&tempTime , NULL);
	srand(tempTime.tv_usec);
	//srand(time(NULL));
	randomValue = rand()%5;
	printf("sleep(%d)\n",randomValue);
	sleep(randomValue);
}